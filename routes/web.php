<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/", function () {
    return redirect('/home');
});
Route::get("/etsyoauth", 'EtsyController@doOauth');
Route::get("/etsyoauthredirect", 'EtsyController@doOauthRedirect');


Auth::routes(['register' => Config::get('app.enable_new_user_registration')]);

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/mapping/{listingId}', 'HomeController@mapping')->name('mapping');
Route::get('/attributes', 'HomeController@attributeRelation')->name('attributeRelation');
Route::get('/transactions', 'HomeController@transactions')->name('transactions');
Route::get('/tracking', 'HomeController@trackings')->name('trackings');
