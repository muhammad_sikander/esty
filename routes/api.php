<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::middleware('auth:api')->get('/etsy2shop/getetsylistingdetails', 'EtsyController@getAllListingDetails');
Route::middleware('auth:api')->get('/etsy2shop/getetsylistings/{page}', 'EtsyController@getAllListings');
Route::middleware('auth:api')->get('/etsy2shop/getetsylistingscount', 'EtsyController@getAllListingsCount');
Route::middleware('auth:api')->get('/etsy2shop/checkoauth', 'EtsyController@checkOauth');
Route::middleware('auth:api')->get('/etsy2shop/getPageTransactions/{page}', 'EtsyController@getPageTransactions');
Route::middleware('auth:api')->get('/etsy2shop/getetsytransactions', 'EtsyController@getTransactions');
Route::middleware('auth:api')->get('/etsy2shop/getPageOrders/{page}', 'EtsyController@getPageOrders');
Route::middleware('auth:api')->get('/etsy2shop/getOrdersCount', 'EtsyController@getAllOrdersCount');
Route::middleware('auth:api')->get('/etsy2shop/getetsylistingimage/{listingId}', 'EtsyController@getListingImage');
Route::middleware('auth:api')->get('/etsy2shop/getTrackingOrders/{page}', 'EtsyController@getTracking');
Route::middleware('auth:api')->get('/etsy2shop/getPageCounts', 'EtsyController@getPageCount');
Route::middleware('auth:api')->get('/etsy2shop/getOrderReceipt', 'EtsyController@getOrderedReceipt');
Route::middleware('auth:api')->get('/etsy2shop/getNewReceipt/{mode}', 'EtsyController@getNewReceipt');
Route::middleware('auth:api')->get('/etsy2shop/updateTracking/{receiptId}', 'EtsyController@updateReceiptTracking');



Route::middleware('auth:api')->get('/etsy2shop/saveetsylistings/{listingId}', 'SavedDataController@storeEtsyListingWithVariants');
Route::middleware('auth:api')->get('/etsy2shop/getsavedlistings', 'SavedDataController@getEtsyListings');
Route::middleware('auth:api')->get('/etsy2shop/getsavedlisting/{listingId}', 'SavedDataController@getEtsyListing');
Route::middleware('auth:api')->get('/etsy2shop/getsavedvariants', 'SavedDataController@getEtsyVariants');
Route::middleware('auth:api')->get('/etsy2shop/getsavedvariant/{variantId}', 'SavedDataController@getEtsyVariant');

//mapping search routes
Route::middleware('auth:api')->get('/etsy2shop/search/id/{listingId}', 'SearchController@searchSingleEtsyListing');
Route::middleware('auth:api')->get('/etsy2shop/search/rid/{orderId}', 'SearchController@searchSingleEstyReceipt');
Route::middleware('auth:api')->get('/etsy2shop/search/recid/{orderId}', 'SearchController@searchSingleTrackingReceipt');
Route::middleware('auth:api')->get('/etsy2shop/search/saved/{page}', 'SearchController@getSavedEtsyListings');
Route::middleware('auth:api')->get('/etsy2shop/search/mapped/{page}', 'SearchController@getMappedEtsyListings');
Route::middleware('auth:api')->get('/etsy2shop/search/notsaved/{page}', 'SearchController@getUnsavedEtsyListings');
Route::middleware('auth:api')->get('/etsy2shop/search/count/{mode}', 'SearchController@geEtsyListingCount');


Route::middleware('auth:api')->post('/etsy2shop/setmapping/{etsyVariantId}/{shopifyVariantId}', 'SavedDataController@updateShopifySkuForVariant');
Route::middleware('auth:api')->get('/etsy2shop/getsavedtransaction/{transactionId}', 'SavedDataController@getTransaction');
//for attribute relations
Route::middleware('auth:api')->get('/etsy2shop/getattributerelations', 'SavedDataController@getAttributeRelations');
Route::middleware('auth:api')->post('/etsy2shop/setattributerelation', 'SavedDataController@saveAttributeRelation');
Route::middleware('auth:api')->post('/etsy2shop/removeattributerelation', 'SavedDataController@deleteAttributeRelation');
Route::middleware('auth:api')->get('/etsy2shop/getvariantmappings/{etsyListingId}/{shopifyProductId}', 'SavedDataController@getEtsyVariantMapFromShopifyProductAndAttributeRelation');


Route::middleware('auth:api')->get('/etsy2shop/getshopifyvariants/{productId}', 'ShopifyController@getShopifyListingsForProductId');
Route::middleware('auth:api')->get('/etsy2shop/getshopifyproductimage/{productId}', 'ShopifyController@getImagesForProductId');

Route::middleware('auth:api')->get('/etsy2shop/createshopifydraft/{etsyTransactionId}', 'ShopifyController@createDraftOrder');
