<?php

return [

    /*
    |--------------------------------------------------------------------------
    | oAuth Config
    |--------------------------------------------------------------------------
    */

    /**
     * Storage
     */
    'storage' => '\\OAuth\\Common\\Storage\\Session',

    /**
     * Consumers
     */
    'consumers' => [

        'Etsy' => [
            'client_id'     => env('ETSY_API_KEY',''),
            'client_secret' => env('ETSY_API_SECRET',''),
            'scope'         => [],
        ],

    ]

];
