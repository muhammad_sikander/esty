<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('variants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('variant_id')->unique();
            $table->integer('listing_id');
//            $table->foreign('listing_id')->references('listing_id')->on('listings');
            $table->string('attribute_1')->default('');
            $table->string('attribute_2')->default('');
            $table->string('etsy_sku')->nullable(true);
            $table->string('shopify_variant_id')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('variants');
    }
}
