<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributeRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attribute_relations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('etsy_attribute_1', 120);
            $table->string('etsy_attribute_2', 120)->default('');
            $table->string('shopify_attribute_1', 120);
            $table->string('shopify_attribute_2', 120)->default('');
            $table->unique(['etsy_attribute_1', 'etsy_attribute_2', 'shopify_attribute_1', 'shopify_attribute_2'],
                'all_items_unique_index');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attribute_relations');
    }
}
