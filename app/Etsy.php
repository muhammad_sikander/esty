<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Ixudra\Curl\Facades\Curl;
use OAuth\Common\Http\Exception\TokenResponseException;
use OAuth\Common\Storage\Exception\TokenNotFoundException;
use App\Shopify;

class Etsy extends Model
{

    private $etsyService;

    /**
     * Etsy constructor.
     */
    public function __construct()
    {
        $this->etsyService = \OAuth::consumer('Etsy');
    }


    /**
     * Checks whether the oauth token for Etsy is valid
     * @return bool
     */
    public function isOauthTokenValid()
    {
        try {
            $this->etsyService->request('/private/users/__SELF__');
            return true;
        } catch (TokenNotFoundException $exception) {
            return false;
        } catch (TokenResponseException $exception) {
            return false;
        }
    }


    /**
     * Gets all listings for this shop including variants for each listing
     * @return Listing
     */
    public function getAllListingDetails($listingId)
    {

        $listingDetails = $this->getSingleListing($listingId);

        if($listingDetails != null) {
            $listing = new Listing();
            $listing->listing_id = $listingDetails['listing_id'];
            $listing->title = $listingDetails['title'];
            $listing->description = $listingDetails['description'];
            if(count($listingDetails['sku']) > 0) {
                $listing->sku = $listingDetails['sku'][0];
            }
            $listing->variants = $this->getVariantsForListing($listingDetails['listing_id']);

            return $listing;
        }

        return null;

    }

     public function getAllReceiptDetails($receiptId)
    {

        $receiptDetails = $this->getSingleReceipt($receiptId);
        if($receiptDetails != null) {

            return $receiptDetails;
        }

        return null;

    }

    /**
     * Gets all listings for this shop (no variant data included)
     * @return array
     */
    public function getAllListings($page)
    {
        $listingDetails = $this->getListingDetailsForShop(Config::get('app.etsy_shop_id'), $page, Config::get('app.mapping_listings_per_page'));
        $listings = array();
        foreach ($listingDetails as $listingDetail) {
            $listing = new Listing();
            $listing->listing_id = $listingDetail['listing_id'];
            $listing->title = $listingDetail['title'];
            $listing->description = $listingDetail['description'];

            array_push($listings, $listing);
        }

        return $listings;
    }

    /**
     * Gets the number of listings that are available in this shop
     */
    public function getAllListingsCount()
    {
        $url = sprintf('https://openapi.etsy.com/v2/shops/%s/listings/active?api_key=%s&page=%d&limit=%d',
            Config::get('app.etsy_shop_id'), Config::get('app.etsy_api_key'), 1, 1);

        $data = Curl::to($url)->get();
        $etsyOutput = json_decode($data, TRUE);

        if (array_key_exists('count', $etsyOutput)) {
            return $etsyOutput['count'];
        }

        return -1;

    }


    public function getOrdersCount()
    {
        if (!$this->isOauthTokenValid()) {
            return null;
        }

        $result = json_decode($this->etsyService->request(
            sprintf('/shops/%s/receipts?limit=500', Config::get("app.etsy_shop_id"))), true);

        if(!is_null($result['results'])) {
            return $result['results'];
        } else {
            return null;
        }


    }


     public function getlistOrders($page)
    {
        if (!$this->isOauthTokenValid()) {
            return null;
        }

        $result = json_decode($this->etsyService->request(
            sprintf('/shops/%s/receipts?limit=10&page='.$page.'', Config::get("app.etsy_shop_id"))), true);

        if(!is_null($result['results'])) {
            return $result['results'];
        } else {
            return null;
        }

    }
    /**
     * Fetches a single listing from Etsy for tracking
     * @param $listingId
     * @return mixed
     */
    public function getTrackingDetails($receiptId)
    {
       if (!$this->isOauthTokenValid()) {
            return null;
        }
        $resultRec = array();
        $etsy = new Etsy();
         $results = json_decode($this->etsyService->request(
            sprintf('/receipts/%s', $receiptId)), true);
         
      foreach ($results['results'] as $value) {
            # code...
            $receiptId = $value['receipt_id']; 
            
   $test = json_decode($this->etsyService->request(
            sprintf('/receipts/%s/transactions', $receiptId)), true);
   $images = array();
            foreach ($test['results'] as $key => $thumb) {
                # code...
                
                 $thumb['thumb'] = (string)$etsy->getImageUrlForListingAndImageId(
                    $thumb['listing_id'], $thumb['image_listing_id']);
                 /*if($thuik != null) {
                 array_push($test['results']['thumb'], $thuik);   
                 }*/
                  array_push($images, $thumb);   
                 
            }
            $test['results'] = [];
            $test['results'] = $images;
            $test['results']['order_id'] = $value['order_id'];
            $test['results']['receipt_id'] = $value['receipt_id']; 
            $test['results']['name'] = $value['name'];
            array_push($resultRec, $test); 
        }
        
        if(!is_null($resultRec) ) {
            return $resultRec;
        } else {
            return null;
        }
    }
    /**
     * Fetches the listings IDs for a given shop ID
     * @param $shopId
     * @return array
     */
    private function getListingDetailsForShop($shopId, $page, $count)
    {
        $listings = array();
        $url = sprintf('https://openapi.etsy.com/v2/shops/%s/listings/active?api_key=%s&page=%d&limit=%d',
            $shopId, Config::get('app.etsy_api_key'), $page, $count);

        $data = Curl::to($url)->get();
        $etsyOutput = json_decode($data, TRUE);

        foreach ($etsyOutput['results'] as $result) {
            $listing = array();
            $listing['listing_id'] = $result['listing_id'];
            $listing['title'] = $result['title'];
            $listing['description'] = $result['description'];
            if(count($result['sku']) > 0) {
                $listing['sku'] = $result['sku'][0];
            }
            array_push($listings, $listing);
        }

        return $listings;
    }

    /**
     * Fetches a single listing from Etsy
     * @param $listingId
     * @return mixed
     */
    private function getSingleListing($listingId)
    {
        $url = sprintf('https://openapi.etsy.com/v2/listings/%s?api_key=%s',
            $listingId, Config::get('app.etsy_api_key'));

        $data = Curl::to($url)->get();
        $etsyOutput = json_decode($data, TRUE);

        if (count($etsyOutput['results']) > 0) {
            return $etsyOutput['results'][0];
        }

        return null;
    }

    
    /**
     * Fetches a single Receipt from Etsy
     * @param $receiptId
     * @return mixed
     */
     private function getSingleReceipt($receiptId)
    {
        if (!$this->isOauthTokenValid()) {
            return null;
        }
        $resultRec = array();
        $etsy = new Etsy();
         $results = json_decode($this->etsyService->request(
            sprintf('/users/%s/receipts', $receiptId)), true);
         
      foreach ($results['results'] as $value) {
            # code...
            $receiptId = $value['receipt_id']; 
            
   $test = json_decode($this->etsyService->request(
            sprintf('/receipts/%s/transactions', $receiptId)), true);
   $images = array();
            foreach ($test['results'] as $key => $thumb) {
                # code...
                
                 $thumb['thumb'] = (string)$etsy->getImageUrlForListingAndImageId(
                    $thumb['listing_id'], $thumb['image_listing_id']);
                 /*if($thuik != null) {
                 array_push($test['results']['thumb'], $thuik);   
                 }*/
                  array_push($images, $thumb);   
                 
            }
            $test['results'] = [];
            $test['results'] = $images;
            $test['results']['order_id'] = $value['order_id'];
            $test['results']['receipt_id'] = $value['receipt_id']; 
            $test['results']['name'] = $value['name'];
            array_push($resultRec, $test); 
        }
        
        if(!is_null($resultRec) ) {
            return $resultRec;
        } else {
            return null;
        }
    }
    /**
     * Fecthes the variants for the given listing id
     * @param $listingId
     * @return array
     */
    private function getVariantsForListing($listingId)
    {
        $url = sprintf('https://openapi.etsy.com/v2/listings/%s/inventory?api_key=%s',
            $listingId, Config::get('app.etsy_api_key'));

        $data = Curl::to($url)->get();
        $etsyOutput = json_decode($data, TRUE);


        $variants = array();
        foreach ($etsyOutput['results']['products'] as $variantEntry) {
//            dd($variantEntry['property_values'][0]);
            $propertyValues = &$variantEntry['property_values'];
            $attributeValues = array();
            foreach ($propertyValues as $property) {
                array_push($attributeValues, $property['values'][0]);
            }
            $variant = new Variant();
            $variant->listing_id = $listingId;
            $variant->variant_id = $variantEntry['product_id'];
            if(count($attributeValues) > 0) {
                $variant->attribute_1 = $attributeValues[0];
            }
            if(count($attributeValues) > 1) {
                $variant->attribute_2 = $attributeValues[1];
            }
            array_push($variants, $variant);

        }

        return $variants;
    }

    /**
     * Fetches the current transactions in Etsy
     * @return |null
     */
    public function getTransactions()
    {    if (!$this->isOauthTokenValid()) {
            return null;
        }
        $resultRec = array();
        $etsy = new Etsy();
        $results = json_decode($this->etsyService->request(
            sprintf('/shops/%s/receipts?limit=500', Config::get("app.etsy_shop_id"))), true);
        foreach ($results['results'] as $value) {
            # code...
            $receiptId = $value['receipt_id']; 
            
   $test = json_decode($this->etsyService->request(
            sprintf('/receipts/%s/transactions', $receiptId)), true);
   $images = array();
            foreach ($test['results'] as $key => $thumb) {
                # code...
                
                 $thumb['thumb'] = (string)$etsy->getImageUrlForListingAndImageId(
                    $thumb['listing_id'], $thumb['image_listing_id']);
                 /*if($thuik != null) {
                 array_push($test['results']['thumb'], $thuik);   
                 }*/
                  array_push($images, $thumb);   
                 
            }
            $test['results'] = [];
            $test['results'] = $images;
            $test['results']['order_id'] = $value['order_id'];
            $test['results']['receipt_id'] = $value['receipt_id']; 
            $test['results']['name'] = $value['name'];
            array_push($resultRec, $test); 
        }
        if(!is_null($resultRec)) {
            return $resultRec;
        } else {
            return null;
        }


    }

    public function getlistTransactions($page)
    {
        if (!$this->isOauthTokenValid()) {
            return null;
        }
        $etsy = new Etsy();
        $resultRec = array();
        $result = json_decode($this->etsyService->request(
            sprintf('/shops/%s/receipts?limit=10&page='.$page.'', Config::get("app.etsy_shop_id"))), true);
         foreach ($result['results'] as $value) {
            # code...
            $receiptId = $value['receipt_id']; 
            $seconds = $value['creation_tsz'];
            $createDate =   date("F d, Y", substr($seconds, 0, 10));; 

            
   $test = json_decode($this->etsyService->request(
            sprintf('/receipts/%s/transactions', $receiptId)), true);
            $images = array();
            foreach ($test['results'] as $key => $thumb) {
                # code...
                
                 $thumb['thumb'] = (string)$etsy->getImageUrlForListingAndImageId(
                    $thumb['listing_id'], $thumb['image_listing_id']);
                 /*if($thuik != null) {
                 array_push($test['results']['thumb'], $thuik);   
                 }*/
                  array_push($images, $thumb);   
                 
            }
            $test['results'] = [];
            $test['results'] = $images;
            $test['results']['order_id'] = $value['order_id'];
            $test['results']['receipt_id'] = $value['receipt_id']; 
            $test['results']['name'] = $value['name'];
            $test['results']['create_date'] = $createDate;
            array_push($resultRec, $test); 
        }
        if(!is_null($resultRec)) {
            //dd($resultRec);
            return $resultRec;
        } else {
            return null;
        }

    }

    /**
     * Gets the transaction details of the Etsy transaction if it exists, or null on non-existence and token invalidity
     * @param $transactionId
     * @return array|null
     */
    public function getTransaction($receiptId)
    {
        if (!$this->isOauthTokenValid()) {
            return null;
        }

        $result =  json_decode($this->etsyService->request(
            sprintf('/receipts/%s/transactions', $receiptId)), true);
        if(!is_null($result['results']) && count($result['results']) > 0) {
            return $result['results'];
        } else {
            return null;
        }
    }

    /**
     * Fetches the receipt information for the given id
     * @param $receiptId
     * @return |null
     */
    public function getReceiptInformation($receiptId)
    {
        if (!$this->isOauthTokenValid()) {
            return null;
        }

        $result = json_decode($this->etsyService->request(
            sprintf('/receipts/%s', $receiptId)), true);
        if(!is_null($result['results']) && count($result['results']) > 0) {
            return $result['results'][0];
        } else {
            return null;
        }
    }
    public function getNewReceipts() 
    {
        $resultRec = array();
        $etsy = new Etsy();
        $result = json_decode($this->etsyService->request(
            sprintf('/shops/%s/receipts/open?limit=500', Config::get("app.etsy_shop_id"))), true);
            foreach ($result['results'] as $value) {
            # code...
            $receiptId = $value['receipt_id']; 
            $seconds = $value['creation_tsz'];
            $createDate =   date("F d, Y", substr($seconds, 0, 10));; 

            
        $test = json_decode($this->etsyService->request(
            sprintf('/receipts/%s/transactions', $receiptId)), true);
        $images = array();
            foreach ($test['results'] as $key => $thumb) {
                # code...
                
                 $thumb['thumb'] = (string)$etsy->getImageUrlForListingAndImageId(
                    $thumb['listing_id'], $thumb['image_listing_id']);
                 /*if($thuik != null) {
                 array_push($test['results']['thumb'], $thuik);   
                 }*/
                  array_push($images, $thumb);   
                 
            }
            $test['results'] = [];
            $test['results'] = $images;
            $test['results']['order_id'] = $value['order_id'];
            $test['results']['receipt_id'] = $value['receipt_id']; 
            $test['results']['name'] = $value['name'];
            $test['results']['create_date'] = $createDate;
            array_push($resultRec, $test); 
        }
        if(!is_null($resultRec)) {
            //dd($resultRec);
            return $resultRec;
        } else {
            return null;
        }
    }


     public function getReceipts($page) 
    {
        $resultRec = array();
        $etsy = new Etsy();
        $result = json_decode($this->etsyService->request(
            sprintf('/shops/%s/receipts/unshipped?limit=10&&page='.$page, Config::get("app.etsy_shop_id"))), true);
            foreach ($result['results'] as $value) {
            # code...
            $receiptId = $value['receipt_id']; 
            $seconds = $value['creation_tsz'];
            $createDate =   date("F d, Y", substr($seconds, 0, 10));; 

            
        $test = json_decode($this->etsyService->request(
            sprintf('/receipts/%s/transactions', $receiptId)), true);
        $images = array();
            foreach ($test['results'] as $key => $thumb) {
                # code...
                
                 $thumb['thumb'] = (string)$etsy->getImageUrlForListingAndImageId(
                    $thumb['listing_id'], $thumb['image_listing_id']);
                 /*if($thuik != null) {
                 array_push($test['results']['thumb'], $thuik);   
                 }*/
                  array_push($images, $thumb);   
                 
            }
            $test['results'] = [];
            $test['results'] = $images;
            $test['results']['order_id'] = $value['order_id'];
            $test['results']['receipt_id'] = $value['receipt_id']; 
            $test['results']['name'] = $value['name'];
            $test['results']['create_date'] = $createDate;
            array_push($resultRec, $test); 
        }
        if(!is_null($resultRec)) {
            //dd($resultRec);
            return $resultRec;
        } else {
            return null;
        }
    }

    public function getPageReceipts()
    {

         if (!$this->isOauthTokenValid()) {
            return null;
        }

        $result = json_decode($this->etsyService->request(
            sprintf('/shops/%s/receipts/unshipped?limit=500', Config::get("app.etsy_shop_id"))), true);

        if(!is_null($result['results'])) {
            return $result['results'];
        } else {
            return null;
        }
    }

    public function getReceiptsTracking($receiptId)
    {
         if (!$this->isOauthTokenValid()) {
            return null;
        }
        $shopify = new Shopify();
        $result = $shopify->getShopifyDetailsForProduct($receiptId);
        if(!is_null($result)) {
            //dd($resultRec);
            return $result;
        } else {
            return null;
        }

    }

    public function submitTracking($receiptId,$company,$tracking)
    {
        
         $result = json_decode($this->etsyService->request(
            sprintf('/shops/%s/receipts/%s/tracking?tracking_code='.$tracking.'&method=POST&carrier_name='.$company, Config::get("app.etsy_shop_id"),$receiptId)), true);

         if(!is_null($result)) {
            //dd($resultRec);
            return $result['results'];
        } else {
            return null;
        }

    }

    public function getCompany($order)
    {
        $country = strtolower($order['shipping_address']['country']);
        $tracking_number = $order['fulfillments'][0]['tracking_number'];
        $tracking_company = "";
        $length = strlen($tracking_number);
        
        $int = "";
        $String = "";
        $laststring = substr($tracking_number, -2);
        $firststring = substr($tracking_number, 0,2);
        
        $first = is_numeric($firststring);
        $last = is_numeric($laststring);
        $intText = is_numeric($tracking_number);

        if($first == false && $last == false )
        {
            $String = true;
        } else {

            $String = false;
        }

        //dd($intText);

        if($country == "united states" && $length == 13 && $intText == false && $String == true) {

            $tracking_company = "usps";
        
        } else if($country == "united kingdom" && $length == 13 && $intText == false && $String == true) {

            $tracking_company = "royal-mail";
        
        } else if($country == "canada" && $length == 13 && $intText == false && $String == true) {

            $tracking_company = "canada-post";
        
        } else if($country == "australia" && $length == 13 && $intText == false && $String == true) {

            $tracking_company = "australia-post";
        
        } else if($country == "france" && $length == 13 && $intText == false && $String == true) {

            $tracking_company = "la-poste";
        
        } else if($country == "italy" && $length == 13 && $intText == false && $String == true) {

            $tracking_company = "poste-italiane";
        
        } else if($country == "netherlands" && $length == 13 && $intText == false && $String == true) {

            $tracking_company = "postnl";
        
        } else if($country == "germany" && $length == 13 && $intText == false && $String == true) {

            $tracking_company = "dhl-germany";
        
        } else if($country == "new zealand" && $length == 13 && $intText == false && $String == true) {

            $tracking_company = "new-zealand-post";
        
        } else if ($country != "united states" && $length == 9 && $intText == true){

            $tracking_company = "tnt";

        } else if ($country == "united states" && $length == 20 && $intText == true){

            $tracking_company = "fedex";

        } else if ($country == "united states" && $length == 12 && $intText == true){

            $tracking_company = "fedex";

        } else if ($country == "united states" && $length == 10 && $intText == true){

            $tracking_company = "dhl";

        } else {

            $tracking_company = "";

        }
        
        return $tracking_company;
    }

    public function getCountryNameForId($countryId)
    {
        $url = sprintf('https://openapi.etsy.com/v2/countries/%s?api_key=%s',
            $countryId, Config::get('app.etsy_api_key'));

        $data = Curl::to($url)->get();
        $etsyOutput = json_decode($data, true);
        if(!is_null($etsyOutput['results']) && count($etsyOutput['results']) > 0) {
            return $etsyOutput['results'][0]['name'];
        }

        return null;
    }

    /**
     * Gets the image URL for the listing for specified image Id
     * @param $listingId
     * @param $imageId
     * @return mixed
     */
    public function getImageUrlForListingAndImageId($listingId, $imageId)
    {
        $url = sprintf('https://openapi.etsy.com/v2/listings/%s/images/%s?api_key=%s',
            $listingId, $imageId, Config::get('app.etsy_api_key'));

        $data = Curl::to($url)->get();
        $etsyOutput = json_decode($data, true);

        return $etsyOutput['results'][0]['url_170x135'];
    }


    /**
     * Gets the image URL for the listing for specified image Id
     * @param $listingId
     * @param $imageId
     * @return mixed
     */
    public function getImageDeailsForListing($listingId)
    {
        $url = sprintf('https://openapi.etsy.com/v2/listings/%s/images?api_key=%s',
            $listingId, Config::get('app.etsy_api_key'));

        $data = Curl::to($url)->get();
        $etsyOutput = json_decode($data, true);
        if(count($etsyOutput['results']) > 0) {
            return $etsyOutput['results'][0];
        } else {
            return null;
        }
    }

    public function getuserInfoFromEtsy($userid)
    {
         
         $result = json_decode($this->etsyService->request(
            sprintf('/users/%s/profile', $userid)),true);
       
         if(!is_null($result)) {
            //dd($result);
            return $result['results'];
        } else {
            return null;
        }
    }

}
