<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Ixudra\Curl\Facades\Curl;
use App\PurchaseMapping;

class Shopify extends Model
{

    /**
     * @param $etsyTransactionId
     * @return PurchaseMapping|null
     */
    public function createTransactionFromEtsyData($etsyTransactionId)
    {
        $etsy = new Etsy();
        $variantArray =  array();
        $userid = '';
        $transactionMapping = new PurchaseMapping();
        $transactionMapping->receipt_id = $etsyTransactionId;

        $etsyTransactionDatas = $etsy->getTransaction($etsyTransactionId);

        //if etsy transaction exists, find the matching shopify sku
        if ($etsyTransactionDatas != null) {
            foreach ($etsyTransactionDatas as $etsyTransactionData) {
                # code...
            $variant = Variant::where('variant_id', $etsyTransactionData['product_data']['product_id'])->first();
            if($variant !=null){
                array_push($variantArray,
                    ['variant_id' => $variant->shopify_variant_id,
                    'quantity' => $etsyTransactionData['quantity']]);
                $userid = $etsyTransactionData['buyer_user_id'];
            } else {

                return response('Mapping Not Found', 404);
            }

            }
            if($variantArray != null) {
                //dd($variantArray);
                //create json body
                $draftOrderData = $this->getShopifyOrderFromEtsyTransactionData(
                    $variantArray,
                    $transactionMapping,
                    $etsyTransactionId,$userid);

              //dd(json_encode($draftOrderData));
                $url = sprintf("https://%s:%s@%s/admin/api/%s/orders.json",
                    Config::get('app.shopify_api_key'), Config::get('app.shopify_api_secret'), Config::get('app.shopify_host_name'),
                    Config::get('app.shopify_api_version'));

                $response = Curl::to($url)
                    ->withData($draftOrderData)
                    ->asJson(true)
                    ->post();
                   //dd($response);
                if(array_key_exists('order', $response)) {
                    $transactionMapping->shopify_order_id = $response['order']['id'];

                    $transactionMapping->save();
                } else {

                    return response('Something Went Wrong', 404);
                }
            } 
 
            return response(['result' => 'OK']); 
    } else {
            //todo throw exception here
           return response('Something Went Wrong', 404);
        }



    }

    public function getShopifyVariantDetailsForProduct($productId)
    {
        $url = sprintf('https://%s:%s@%s/admin/api/%s/products/%s/variants.json?limit=%s',
            Config::get('app.shopify_api_key'), Config::get('app.shopify_api_secret'), Config::get('app.shopify_host_name'),
            Config::get('app.shopify_api_version'), $productId, Config::get('app.shopify_page_limit'));

        $response = Curl::to($url)
            ->asJson(true)
            ->get();

        if(array_key_exists('variants', $response)) {
            return $response['variants'];
        } else {
            return null;
        }


    }

    public function getShopifyDetailsForProduct($orderId)
    {
        //dd($orderId);
        $url = sprintf('https://%s:%s@%s/admin/api/2019-10/orders/%s.json',
            Config::get('app.shopify_api_key'), Config::get('app.shopify_api_secret'), Config::get('app.shopify_host_name'), $orderId);
       
        $response = Curl::to($url)
            ->asJson(true)
            ->get();

        if( $response != null) {
            return $response['order'];
        } else {
            return null;
        }


    }

    /**
     * Creates the shopify order array from etsy data
     * @param $etsyTransaction
     * @return array|null
     */
    private function getShopifyOrderFromEtsyTransactionData($shopifyVariantId,PurchaseMapping $transactionMapping, $receiptId,$userid)
    {
        $draftOrder = array();
        $shopify = new Shopify();
        $draftOrder['order']['line_items'] = $shopifyVariantId;
       // array_push($draftOrder['order']['line_items'],$shopifyVariantId);
        $draftOrder['order']['shipping_address'] = $transactionMapping
            ->getShopifyShippingInfoFromEtsyReceipt($receiptId);
        $draftOrder['order']['customer'] = $transactionMapping->getuserInfoFromEtsyReceipt($userid);

        return $draftOrder;
    }

    /**
     * Gets a list of images for a given product id in shopify
     */
    public function getImagesForProductId($productId)
    {
        $url = sprintf('https://%s:%s@%s/admin/api/%s/products/%s/images.json',
            Config::get('app.shopify_api_key'), Config::get('app.shopify_api_secret'), Config::get('app.shopify_host_name'),
            Config::get('app.shopify_api_version'), $productId);
        $response = Curl::to($url)
            ->asJson(true)
            ->get();
        if(array_key_exists('images', $response)) {
            return $response['images'];
        } else {
            return null;
        }
    }

}
