<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Variant extends Model
{
    protected $table = 'variants';
    protected $fillable = ['listing_id', 'variant_id', 'attribute_1', 'attribute_2', 'shopify_variant_id'];
    protected $visible = ['listing_id', 'variant_id', 'attribute_1', 'attribute_2', 'shopify_variant_id'];
}
