<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseMapping extends Model
{
    protected $table = "purchase_mappings";
    protected $fillable = ['transaction_id', 'price', 'quantity', 'variant_id', 'title', 'thumbnail', 'shopify_order_id'];
    protected $visible = ['transaction_id', 'price', 'quantity', 'variant_id', 'title', 'thumbnail', 'shopify_order_id','reciept_id'];




    public function getShopifyShippingInfoFromEtsyReceipt($receiptId) {
        $etsy = new Etsy();
        $receiptInfo = $etsy->getReceiptInformation($receiptId);
        if (!is_null($receiptInfo)) {
            $country = $etsy->getCountryNameForId($receiptInfo['country_id']);
            return [
                'first_name' => html_entity_decode(preg_replace('/^(.+) (.+)/i', '${1}', $receiptInfo['name']), ENT_QUOTES),
                'last_name' => html_entity_decode(preg_replace('/^(.+) (.+)/i', '${2}', $receiptInfo['name']), ENT_QUOTES),
                'address1' => html_entity_decode($receiptInfo['first_line'], ENT_QUOTES),
                'address2' => html_entity_decode($receiptInfo['second_line'], ENT_QUOTES),
                'city' => html_entity_decode($receiptInfo['city'], ENT_QUOTES),
                'province' => html_entity_decode($receiptInfo['state'], ENT_QUOTES),
                'zip' => $receiptInfo['zip'],
                'country' => html_entity_decode($country, ENT_QUOTES)
            ];
        }
        else {
            return null;
        }
    }

    public function getuserInfoFromEtsyReceipt($userid)
    {
        $etsy = new Etsy();
        $userinfo = $etsy->getuserInfoFromEtsy($userid);
        if($userinfo !=null){

            return [
                'first_name' => $userinfo[0]['first_name']
            ];
        } else {

            return null;
        }
    }
}
