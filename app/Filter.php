<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Filter extends Model
{

    /**
     * Filters out only the listings which are saved in the database
     */
    public function getSavedEtsyListings()
    {
        return Listing::all();
    }

    /**
     * Filters out the saved and mapped listings from the database
     */
    public function getMappedEtsyListings()
    {
        $ouput = array();
        $listings = $this->getSavedEtsyListings();
        foreach ($listings as $listing) {
            $mappedVariant = Variant::where('listing_id', $listing->listing_id)
                ->whereNotNull('shopify_variant_id');
            if($mappedVariant != null) {
                array_push($ouput, $listing);
            }
        }
    }

    public function getUnsavedEtsyListings()
    {
        $etsy = new Etsy();
        $allListingsInEtsy = $etsy->getAllListings();
        $savedListings = $this->getSavedEtsyListings();

        $allItemIdsArray = array();
        $savedItemIdsArray = array();

        foreach($allListingsInEtsy as $listing) {
            array_push($allItemIdsArray, $listing->listing_id);
        }

        foreach ($savedListings as $listing) {
            array_push($savedItemIdsArray, $listing->listing_id);
        }
    }
}
