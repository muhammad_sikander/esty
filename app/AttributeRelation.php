<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttributeRelation extends Model
{
    protected $table = "attribute_relations";
    protected $fillable = ['etsy_attribute_1', 'etsy_attribute_2', 'shopify_attribute_1', 'shopify_attribute_2'];
    protected $visible = ['id', 'etsy_attribute_1', 'etsy_attribute_2', 'shopify_attribute_1', 'shopify_attribute_2'];
}
