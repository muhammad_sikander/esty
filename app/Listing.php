<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Listing extends Model
{

    protected $table = 'listings';
    protected $fillable = ['listing_id', 'title', 'description', 'sku'];
    protected $visible = ['listing_id', 'title', 'description', 'sku', 'variants'];


}
