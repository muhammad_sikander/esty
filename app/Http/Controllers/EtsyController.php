<?php

namespace App\Http\Controllers;

use App\Etsy;
use App\Listing;
use App\PurchaseMapping;
use App\Variant;
use DB;
use Illuminate\Support\Facades\Config;
use Ixudra\Curl\Facades\Curl;
use OAuth\Common\Storage\Exception\TokenNotFoundException;

class EtsyController extends Controller
{
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * handles /etsy2shop/getetsylistingdetails
     * @return Listing
     */
    public function getAllListingDetails() {

        $etsy = new Etsy();
        return $etsy->getAllListingDetails(1);
    }

    /**
     * handles /etsy2shop/getetsylistings
     * @param $page page number to fetch
     * @return array
     */
    public function getAllListings($page)
    {
        $etsy = new Etsy();
        return $etsy->getAllListings($page);
    }


    public function getAllListingsCount()
    {
        $etsy = new Etsy();
        return ['count' => $etsy->getAllListingsCount()];

    }

    public function getAllOrdersCount()
    {
        $etsy = new Etsy();
        $transactions = $etsy->getOrdersCount();
        if($transactions != null) {
           
            return $transactions;

        } else {
            return response('Token Expired', 403);
        }
    } 

    public function getPageOrders($page)
    {
        $etsy = new Etsy();
        $transactions = $etsy->getlistOrders($page);
        if($transactions != null) {
           
            return $transactions;

        } else {
            return response('Token Expired', 403);
        }
    }

    /**
     * handlses /etsy2shop/checkoauth
     * Checks whether the oauth token is saved or not. This can be used for the front end to driect users
     *   towards the oauth back-end path
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function checkOauth()
    {
        $etsy = new Etsy();
        if($etsy->isOauthTokenValid()) {
            return response('', 200);
        } else {
            return response('', 401);
        }
    }




    /**
     * handles /etsyoauth
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function doOauth()
    {
        $etsy = \OAuth::consumer('Etsy', Config::get("app.url") .'/etsyoauthredirect');

        $response = $etsy->requestRequestToken();
        $requestTokenSecret = $response->getRequestTokenSecret();
        session()->put('request_token_secret', $requestTokenSecret);
        $extra = $response->getExtraParams();

        return redirect($extra['login_url']);
    }

    /**
     * handles /etsyoauthredirect
     * handles the redirect from etsy and moves user back to the front end langing page
     */
    public function doOauthRedirect()
    {
        $etsy = \OAuth::consumer('Etsy');
        $requestTokenSecret = session()->get('request_token_secret');
        $accessToken = $etsy->requestAccessToken($_GET['oauth_token'], $_GET['oauth_verifier'], $requestTokenSecret);

        return redirect(Config::get('app.url') . Config::get('app.frontend_landing_page'));
    }


    /**
     * handles /etsy2shop/getetsytransactions
     */
    public function getTransactions()
    {
        $etsy = new Etsy();

        $transactions = $etsy->getTransactions();
        if($transactions != null) {
           
            return $transactions;

        } else {
            return response('Token Expired', 403);
        }
    }

     public function getPageTransactions($page)
    {
        $etsy = new Etsy();

        $transactions = $etsy->getlistTransactions($page);

        $mappings = array();
        if($transactions != null) {
            foreach ($transactions as $transaction) {
               /* $purchaseMapping = new PurchaseMapping();
                $purchaseMapping->transaction_id = $transaction['transaction_id'];
                $purchaseMapping->price = $transaction['price'];
                $purchaseMapping->quantity = $transaction['quantity'];
                $purchaseMapping->product_ref = $transaction['product_data']['product_id'];
                $purchaseMapping->title = $transaction['title'];
                $purchaseMapping->thumbnail = $etsy->getImageUrlForListingAndImageId(
                    $transaction['listing_id'], $transaction['image_listing_id']);*/
               /* $thumbnail = $etsy->getImageUrlForListingAndImageId(
                    $transaction['results']['listing_id'], $transaction['results']['image_listing_id']);*/
                 $variant = PurchaseMapping::where('receipt_id', $transaction['results']['receipt_id'])->first();
                if($variant == null) {
                //$transaction['results']['image'] = $thumbnail;
                array_push($mappings, $transaction['results']);
                }
            }
            
            return $mappings;

        } else {
            return response('Token Expired', 403);
        }
    }

    private function getReceiptInfo($receiptId)
    {
        $etsy = \OAuth::consumer('Etsy');
        try {
            $result = json_decode($etsy->request(sprintf('/receipts/%s', $receiptId)));
        } catch (TokenNotFoundException $exception) {
            return null;
        }
    }



    public function getListingImage($listingId)
    {
        $etsy = new Etsy();
        return $etsy->getImageDeailsForListing($listingId);
    }
    public function getNewReceipt($mode) 
    {

        $etsy = new Etsy();

        $transactions = $etsy->getNewReceipts($mode);

        $mappings = array();
        if($transactions != null) {
            foreach ($transactions as $transaction) {

            $variant = PurchaseMapping::where('receipt_id', $transaction['results']['receipt_id'])->first();
            if($variant == null) {
             array_push($mappings, $transaction['results']);
            }
            }

            return $mappings;

        } else {
            return response('Token Expired', 403);
        }

    }
    /*Tracking page*/

    public function getTracking($page)
    {
         $etsy = new Etsy();

        $transactions = $etsy->getReceipts($page);

        $mappings = array();
        if($transactions != null) {
            foreach ($transactions as $transaction) {

             array_push($mappings, $transaction['results']);
            }

            return $mappings;

        } else {
            return response('Token Expired', 403);
        }
            //$trackings = PurchaseMapping::get()->groupBy('receipt_id');
        return $trackings;
    }

/*Pagination tracking*/

 public function getPageCount()
    {
         $etsy = new Etsy();

        $transactions = $etsy->getPageReceipts();

        if($transactions != null) {
         
            return $transactions;

        } else {
            return response('Token Expired', 403);
        }
        return $trackings;
    }

    public function getOrderedReceipt()
    {
         $etsy = new Etsy();
         $mode = 'New';
        $transactions = $etsy->getNewReceipts($mode);

        $mappings = array();
        if($transactions != null) {
            foreach ($transactions as $transaction) {

            $variant = PurchaseMapping::where('receipt_id', $transaction['results']['receipt_id'])->where('shopify_order_id','!=','')->first();
            if($variant != null) {
             array_push($mappings, $transaction['results']);
            }
            }

            return $mappings;

        } else {
            return response('Token Expired', 403);
        }

        return $trackings;
    }

    public function updateReceiptTracking($receiptId)
    {
        $shopify_order_id = PurchaseMapping::where('receipt_id', $receiptId)->whereNotNull('shopify_order_id')->first();
        $etsy = new Etsy();
        $tracking = $etsy->getReceiptsTracking($shopify_order_id['shopify_order_id']);
        $tracking_company = $etsy->getCompany($tracking);

        $tracking_number = $tracking['fulfillments'][0]['tracking_number'];

        $submitTracking = $etsy->submitTracking($receiptId,$tracking_company,$tracking_number);
        
        if($submitTracking != null) {
            return $submitTracking;
        } else {
            return response('Token Expired', 403);
        }

    }


}
