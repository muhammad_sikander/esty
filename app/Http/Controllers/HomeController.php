<?php

namespace App\Http\Controllers;

use App\Etsy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function apiTokenUpdate(Request $request)
    {
        $token = Str::random(60);

        $request->user()->forceFill([
            'api_token' => hash('sha256', $token),
        ])->save();

        return ['token' => $token];
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $token = $this->apiTokenUpdate($request);
        $request->session()->put('api_token', $token);

        return view('home');
    }



    public function mapping($listingId)
    {
        return view('mapping', compact('listingId'));
    }


    public function transactions(Request $request)
    {
        $etsyService = new Etsy();

        if($etsyService->isOauthTokenValid()) {
            $token = $this->apiTokenUpdate($request);
            $request->session()->put('api_token', $token);
            return view('transactions', ['token' => true]);
        } else {
            return view('transactions', ['token' => false]);
        }
    }

     public function trackings(Request $request)
    {
        $etsyService = new Etsy();

        if($etsyService->isOauthTokenValid()) {
            $token = $this->apiTokenUpdate($request);
            $request->session()->put('api_token', $token);
            return view('tracking', ['token' => true]);
        } else {
            return view('tracking', ['token' => false]);
        }
    }

    public function attributeRelation()
    {
        return view('attributerelation');
    }
}
