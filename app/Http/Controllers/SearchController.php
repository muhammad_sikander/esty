<?php

namespace App\Http\Controllers;

use App\Etsy;
use App\Listing;
use App\Variant;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{

    /**
     * Fetches a single listing when searched
     */
    public function searchSingleEtsyListing($listingId) {
        $etsy = new Etsy();
        return $etsy->getAllListingDetails($listingId);
    }

    /**
     * Fetches a single reciept when searched
     */
    public function searchSingleEstyReceipt($receiptId) {
        $etsy = new Etsy();
        $transactions = $etsy->getAllReceiptDetails($receiptId);
         $mappings = array();
        if($transactions != null) {
            foreach ($transactions as $transaction) {

               array_push($mappings, $transaction['results']);
            }

            return $mappings;

        } else {
            return response('Token Expired', 403);
        }
    }

       /**
     * Fetches a single reciept For tracking
     */
    public function searchSingleTrackingReceipt($receiptId) {
        $etsy = new Etsy();
        $transactions = $etsy->getTrackingDetails($receiptId);
         $mappings = array();
        if($transactions != null) {
            foreach ($transactions as $transaction) {

               array_push($mappings, $transaction['results']);
            }

            return $mappings;

        } else {
            return response('Token Expired', 403);
        }
    }
    /**
     * Filters out only the listings which are saved in the database
     */
    public function getSavedEtsyListings($page)
    {
        $skips = ($page - 1) * Config::get('app.mapping_listings_per_page');
        return Listing::skip($skips)->take(Config::get('app.mapping_listings_per_page'))->get();
    }

    /**
     * Filters out the saved and mapped listings from the database
     */
    public function getMappedEtsyListings($page)
    {
        $skips = ($page - 1) * Config::get('app.mapping_listings_per_page');
        $limit = Config::get('app.mapping_listings_per_page');
        $mappedListings = DB::select(
            'select * from listings where listing_id in (select listing_id from variants where shopify_variant_id is not null group by listing_id) limit '. $skips .','. $limit);
        $listings = Listing::hydrate($mappedListings);
        return $listings;

    }


    /**
     * handles /etsy2shop/search/notsaved/{page}
     * we only return the unsaved listings of this page. If a page has all listings mapped, 
     *   the returning array would be empty. Its the JS's job to collect everything and make a page
     *   full of data
     */
    public function getUnsavedEtsyListings($page) {
        $unmappedListings = array();
        $etsy = new Etsy();
        $allListingsInPage = $etsy->getallListings($page);
        foreach($allListingsInPage as $listing) {
            $count = Listing::where('listing_id', $listing->listing_id)->count();
            if($count == 0) {
                array_push($unmappedListings, $listing);
            }
        }
        
        return $unmappedListings;
        
    }

    public function geEtsyListingCount($mode)
    {
        if ($mode == "saved") {
            return ['count' => Listing::count()];
        } elseif ($mode == "mapped") {
            return ['count' => Variant::whereNotNull('shopify_variant_id')->groupBy('listing_id')->get()->count()];
        } elseif ($mode == "notsaved") {
            $etsy = new Etsy();
            return ['count' => $etsy->getAllListingsCount() - Listing::count()];
        }
    }
}
