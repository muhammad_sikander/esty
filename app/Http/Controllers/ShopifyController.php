<?php

namespace App\Http\Controllers;

use App\PurchaseMapping;
use App\Shopify;
use Illuminate\Http\Request;

class ShopifyController extends Controller
{

    public function createDraftOrder($etsyTransactionId)
    {
        $shopifyService = new Shopify();
        $result = $shopifyService->createTransactionFromEtsyData($etsyTransactionId);

        /*if($result->shopify_order_id != null) {
            $result->save();
            return $result;
        } else {
            return response(['status' => 'error', 'message' => 'order not created'], 403);
        }*/

        return $result;
    }

    public function getShopifyListingsForProductId($productId)
    {
        $shopifyService = new Shopify();
        $productDetails = $shopifyService->getShopifyVariantDetailsForProduct($productId);
        if($productDetails != null) {
            return $productDetails;
        } else {
            return response(['status'=> 'error'], 404);
        }
    }

    public function getImagesForProductId($productId)
    {
        $shopifyService = new Shopify();
        return $shopifyService->getImagesForProductId($productId);
    }
}
