<?php

namespace App\Http\Controllers;

use App\Etsy;
use App\Listing;
use App\PurchaseMapping;
use App\AttributeRelation;
use App\Shopify;
use App\Variant;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

class SavedDataController extends Controller
{
    /**
     * Serves /etsy2shop/saveetsylistings
     * @param Request $request
     * @return string
     */
    public function storeEtsyListingWithVariants($listingId)
    {
        $etsy = new Etsy();
        $listingItem = $etsy->getAllListingDetails($listingId);
        
        if($listingItem != null) {
            //this is it, make sure to remove the variants before saving.
            //  For this purpose, we clone the item
            $backupItem = clone $listingItem;
            unset($backupItem->variants);
            $backupItem->save();
            //use the original item for parsing thru the variants
            foreach ($listingItem->variants as $variantItem) {
                $variantItem->save();
            }

            return response(['result' => 'OK']);
        }

        return response(['result' => 'FAIL'], 503);

    }

    /**
     * Gets all stored Etsy listings in the listings table
     * Serves /etsy2shop/getsavedlistings
     * @return Listing[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getEtsyListings()
    {
        $listings =  Listing::all();
        foreach ($listings as &$listing) {
            $listing->variants = Variant::where('listing_id', $listing->listing_id)->get();

        }
        return $listings;
    }


    /**
     * handles /etsy2shop/getsavedlisting/{listingId}
     * @param $listingId
     * @return mixed
     */
    public function getEtsyListing($listingId) {
        $listing = Listing::where('listing_id', $listingId)->first();
        if($listing != null) {
            $listing->variants = Variant::where('listing_id', $listingId)->get();
            return $listing;
        }

        return response('', 404);
    }

    /**
     * Gets all stored variants in the variants table. Records from multiple listings will appear
     * Serves /etsy2shop/getetsyvariants
     * @return Variant[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getEtsyVariants()
    {
        return Variant::all()->makeVisible(['variant_id', 'listing_id', 'attribute_1', 'attribute_2']);
    }


    /**
     * Fetches the variant for the given product reference (variant id)
     * @param $variantId
     * @return mixed
     */
    public function getEtsyVariant($variantId)
    {
        return Variant::where('variant_id', $variantId)->first();
    }



    public function updateShopifySkuForVariant($etsyVariantId, $shopifyVariantId)
    {
        $variant = Variant::where('variant_id', $etsyVariantId)->first();
        if($variant != null) {
            $variant->shopify_variant_id = $shopifyVariantId;
            $variant->save();


            return response()->json([
                'success'=> true
            ]);

        } else {
            return response()->json([
                'success' => false
            ]);
        }
    }


    /**
     * handles /api/etsy2shop/getattributerelations
     *
     */
    public function getAttributeRelations()
    {
        return AttributeRelation::all();

    }

    /**
     * handles /etsy2shop/setattributerelation/
     */
    public function saveAttributeRelation()
    {
        //get the post body content
        $relationData = request()->all();
        //try to create the record.
        try {
            $attributeRelation = new AttributeRelation();
            $attributeRelation->fill($relationData);
            $result = $attributeRelation->save();
            if($result) {
                return response()->json([
                    'success' => true
                ]);
            } else {
                return response()->json([
                    'success' => false
                ]);
            }
        } catch (QueryException $e) {
            return response()->json([
                'success' => false
            ]);
        }


    }

    /**
     * handles /etsy2shop/removeattributerelation
     * send etsy_attribute and etsy_value in json body
     */
    public function deleteAttributeRelation()
    {
        $relationData = request()->all();
        $attributeRelation = AttributeRelation::find($relationData['id']);
        $attributeRelation->delete();

    }


    /**
     * handles /etsy2shop/getvariantmappings/{etsyListingId}/{shopifyProductId}
     * Finds variant options from Etsy and Shopify for given product Ids which has a matching entry
     *   in our attribute relations table.
     * @param $etsyListingId
     * @param $shopifyProductId
     * @return array|null
     */
    public function getEtsyVariantMapFromShopifyProductAndAttributeRelation($etsyListingId, $shopifyProductId)
    {
        //find the shopify variants for the shopify product Id
        $shopifyService = new Shopify();
        $shopifyVariants = $shopifyService->getShopifyVariantDetailsForProduct($shopifyProductId);
        $etsyVariants = Variant::where('listing_id', $etsyListingId)->get();

//        dd($shopifyVariants);
//        dd($etsyVariants);

        if($shopifyVariants != null) {
            $matches = array();     //array to store all the matched variants (as 'shopify_variant', 'etsy_variant')

            foreach ($shopifyVariants as $shopifyVariant) {
                foreach ($etsyVariants as $etsyVariant) {
                    $matchedRelation = AttributeRelation::where('shopify_attribute_1', $shopifyVariant['option1'])
                        ->where('shopify_attribute_2', $shopifyVariant['option2'] . '')
                        ->where('etsy_attribute_1', html_entity_decode($etsyVariant['attribute_1'], ENT_QUOTES))
                        ->where('etsy_attribute_2', html_entity_decode($etsyVariant['attribute_2'], ENT_QUOTES))
                        ->first();


                    if($matchedRelation != null) {
                        $item = array();
                        $item['etsy_variant'] = $etsyVariant['variant_id'];
                        $item['shopify_variant'] = $shopifyVariant['id'];

                        array_push($matches, $item);
                    }
                }

            }
            return $matches;

        } else {
            //there is no such shopify product Id
            return null;
        }
    }


    /**
     * Gets the mapped transaction from purchase mappings.
     * @param $transactionId
     * @return PurchaseMapping
     */
    public function getTransaction($transactionId)
    {
        $mapping = PurchaseMapping::where('receipt_id', $transactionId)->first();
        if ($mapping != null) {
            return $mapping;
        } else {
            return response('', 404);
        }
    }



}
