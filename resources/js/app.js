/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


require('./bootstrap');

var Paginate = require('vuejs-paginate');
window.Vue = require('vue');
import FlashMessage from '@smartweb/vue-flash-message';
Vue.use(FlashMessage);
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */
Vue.component('paginate', Paginate);
// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('home-component', require('./components/HomeComponent.vue').default);
Vue.component('mapping-status-item', require('./components/EtsyMappingStatusItemComponent.vue').default);


//components mapping etsy attributes to shopify attributes
Vue.component('attribute-relation-container', require('./components/AttributeRelationContainerComponent.vue').default);
Vue.component('attribute-relation-item', require('./components/AttributeRelationItemComponent.vue').default);

Vue.component('mapping-item', require('./components/MappingItemComponent.vue').default);
Vue.component('mapping-items-container', require('./components/MappingItemsContainer.vue').default);
Vue.component('transaction-item', require('./components/TransactionMappingItemComponent.vue').default);
Vue.component('transaction-container', require('./components/TransactionMappingContainerComponent.vue').default);
Vue.component('tracking-item', require('./components/TrackingMappingItemComponent.vue').default);
Vue.component('tracking-container', require('./components/TrackingMappingContainerComponent.vue').default);
Vue.component('redirection-counter', require('./components/RedirectionCountdownComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
