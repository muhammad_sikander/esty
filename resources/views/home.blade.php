@extends('layouts.app')



@section('content')
        <home-component></home-component>
@endsection
<script>
    window.Laravel = {!! json_encode([
       'csrfToken' => csrf_token(),
       'apiToken' => session('api_token', 'default'),
   ]) !!};
</script>
<script>
    import HomeComponent from "../js/components/HomeComponent";
    export default {
        components: {HomeComponent}
    }
</script>
