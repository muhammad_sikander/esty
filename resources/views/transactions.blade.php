@extends('layouts.app')

@section('content')
    @if($token)
        <transaction-container></transaction-container>
    @else
        <redirection-counter></redirection-counter>
    @endif
@endsection
<script>
    window.Laravel = {!! json_encode([
       'csrfToken' => csrf_token(),
       'apiToken' => session('api_token', 'default'),
   ]) !!};
</script>
