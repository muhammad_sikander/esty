@extends('layouts.app')

@section('content')
    <attribute-relation-container></attribute-relation-container>
@endsection
<script>
    window.Laravel = {!! json_encode([
       'csrfToken' => csrf_token(),
       'apiToken' => session('api_token', 'default'),
   ]) !!};
</script>