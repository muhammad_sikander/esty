@extends('layouts.app')

@section('content')
    <mapping-items-container listing-id="{{$listingId}}"></mapping-items-container>
@endsection
<script>
    window.Laravel = {!! json_encode([
       'csrfToken' => csrf_token(),
       'apiToken' => session('api_token', 'default'),
   ]) !!};
</script>
